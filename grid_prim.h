#ifndef GRID_PRIM_H
#define GRID_PRIM_H

#include "hanan_grid.h"

Coord mst(const struct grid *G, Set S);

#endif // GRID_PRIM_H
