#include "hanan_grid.h"

int main(int argc, char *argv[])
{
	if (argc < 2) return 1;

	FILE *file = fopen(argv[1], "r");
	struct grid G = read_grid_from_file(file);
	fclose(file);

	print_grid(&G);

	printf("Distance between first two terminals is ");
	printf("%ld\n", dist(&G,
			     vertex_id_to_tuple_id(&G, G.terminals[0]),
			     vertex_id_to_tuple_id(&G, G.terminals[1]))
	);

	free_grid(&G);

	return 0;
}
