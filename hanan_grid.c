#include "hanan_grid.h"

#include <stdlib.h>

void add_pathsetvector(struct pathsetvector* P, const struct pathset w)
{
	if (!P->max_num_pathsets) {
		P->vector = malloc(sizeof(w));
		P->max_num_pathsets = 1;
	} else while (P->max_num_pathsets <= P->num_pathsets) {
		P->vector = reallocarray(
			P->vector,
			P->max_num_pathsets <<= 1,
			sizeof(w)
		);
	}
	P->vector[P->num_pathsets++] = w;
}

void add_to_P(const struct grid* G, const VertexId v, const Set I, const Coord key, const Coord lb)
{
	const struct pathset init = {.I=I, .key=key, .lb=lb};
	add_pathsetvector(&G->path_sets[v], init);
	G->index_of_pathsets[v][I] = G->path_sets[v].num_pathsets;
}


/*
 * Vertex stuff
 */

VertexId tuple_id_to_vertex_id(const struct grid *G, const struct tuple_id v)
{
	VertexId vertex_id = 0;
	for (Dim d = DIM-1; d >= 0; --d) {
		vertex_id *= G->num_vertices[d];
		vertex_id += v.id[d];
	}
	return vertex_id;
}

struct tuple_id vertex_id_to_tuple_id(const struct grid *G, VertexId vertex_id)
{
	struct tuple_id tuple_id;
	for (Dim i = 0; i < DIM; ++i) {
		tuple_id.id[i] = vertex_id % G->num_vertices[i];
		vertex_id /= G->num_vertices[i];
	}
	return tuple_id;
}

struct pos position_of_vertex_by_tuple_id(const struct grid *G, const struct tuple_id v)
{
	struct pos position;
	for (Dim i = 0; i < DIM; ++i) {
		position.coord[i] = G->positions[i][v.id[i]];
	}
	return position;
}

struct pos position_of_vertex_by_vertex_id(const struct grid *G, const VertexId v)
{
	return position_of_vertex_by_tuple_id(G, vertex_id_to_tuple_id(G, v));
}

/*
 * Grid stuff
 */

Coord dist(const struct grid *G, const struct tuple_id v, const struct tuple_id w)
{
	Coord ans = 0;
	for (Dim d = 0; d < DIM; ++d) {
		ans += labs(G->positions[d][w.id[d]] - G->positions[d][v.id[d]]);
	}
	return ans;
}

void free_grid(struct grid *G)
{
	for (Dim i = 0; i < DIM; ++i) {
		G->num_vertices[i] = 0;
		free(G->positions[i]);
	}

	for (VertexId v = 0; v < G->total_num_vertices; ++v) {
		free(G->path_sets[v].vector);
		free(G->fib_sets[v]);
		free(G->index_of_pathsets[v]);
	}

	free(G->path_sets);
	free(G->fib_sets);
	free(G->index_of_pathsets);

	G->path_sets = NULL;
	G->fib_sets = NULL;
	G->num_terminals = 0;
	G->total_num_vertices = 0;
}

void print_grid(const struct grid *G)
{
	printf("Number of vertices = %u\n\n", G->total_num_vertices);
	for (Dim d = 0; d < DIM; ++d) {
		printf("%u Positions in direction %d:", G->num_vertices[d], d);
		for (VertexId i = 0; i < G->num_vertices[d]; ++i)
			printf(" %d", G->positions[d][i]);
		printf("\n");
	}
	printf("\nTerminals:");
	for (TerminalId t = 0; t < G->num_terminals; ++t) {
		struct pos terminal_pos = position_of_vertex_by_vertex_id(G,G->terminals[t]);
		printf(" (");
		for (Dim d = 0; d < DIM-1; ++d)
			printf("%d, ", terminal_pos.coord[d]);
		printf("%d)", terminal_pos.coord[DIM-1]);
	}
	printf("\n");
}

struct grid read_grid_from_file(FILE *stream)
{
	if (stream == NULL) {
		fprintf(stderr, "Invalid file stream.\n");
		exit(1);
	}

	int n; // number of terminals
	if (fscanf(stream, "%d\n", &n) != 1) {
		fprintf(stderr, "Bad first line!\n");
		exit(1);
	}

	struct pos *terminals = malloc(n * sizeof(struct pos));

	for (TerminalId i = 0; i < n; ++i) {
		for (Dim d = 0; d < DIM-1; ++d) {
			if (fscanf(stream, "%d ", &(terminals[i].coord[d])) != 1) {
				fprintf(stderr, "l. %d: Bad line!\n", i+2);
				exit(1);
			}
		}
		if (fscanf(stream, "%d\n", &(terminals[i].coord[DIM-1])) != 1) {
			fprintf(stderr, "l. %d: Bad line!\n", i+2);
			exit(1);
		}
	}

	struct grid G = {
		.num_vertices = {0},
		.num_terminals = n
	};
	for (Dim d = 0; d < DIM; ++d) {
		G.positions[d] = malloc(n * sizeof(Coord));
	}

	struct tuple_id *terminal_tuple_ids = malloc(n * sizeof(struct tuple_id));

	int *natural_numbers = malloc(n * sizeof(int)); // workaround since fibonacci heaps want
	                                                // (void *) as values, and not int
	for (TerminalId i = 0; i < n; ++i)
		natural_numbers[i] = i;
	struct fib_heap heap = {.n = 0, .b = NULL}; // for sorting terminals
	for (Dim d = 0; d < DIM; ++d) {
		for (TerminalId i = 0; i < n; ++i) {
			fib_heap_insert(&heap, natural_numbers+i, terminals[i].coord[d]);
		}
		VertexId num_coords = 0;
		for (TerminalId i = 0; i < n; ++i) {
			struct fib_node *node = fib_heap_extract_min(&heap);
			if ((num_coords == 0) || (node->key > G.positions[d][num_coords-1]))
				G.positions[d][num_coords++] = node->key;
			terminal_tuple_ids[*((int *) node->val)].id[d] = num_coords-1;
			free(node);
		}
		G.num_vertices[d] = num_coords;
	}
	fib_heap_free(&heap);
	free(natural_numbers);
	
	G.total_num_vertices = 1;
	for (Dim d = 0; d < DIM; ++d)
		G.total_num_vertices *= G.num_vertices[d];
	for (TerminalId i = 0; i < n; ++i)
		G.terminals[i] = tuple_id_to_vertex_id(&G, terminal_tuple_ids[i]);

	// Cache terminal distances for faster MST calculation. Hopefully :(
	for (TerminalId i = 0; i < n; ++i)
		for (TerminalId j = 0; j < n; ++j)
			G.terminal_dist[i][j] =
				dist(&G, terminal_tuple_ids[i], terminal_tuple_ids[j]);

	G.fib_sets = malloc(G.total_num_vertices * sizeof(struct fib_node**));
	G.path_sets = calloc(G.total_num_vertices , sizeof(struct pathsetvector));
	G.index_of_pathsets = malloc(G.total_num_vertices * sizeof(size_t*));
	for (VertexId v = 0; v < G.total_num_vertices; ++v) {
		G.index_of_pathsets[v] = calloc(1<<n, sizeof(size_t));
		G.fib_sets[v] = calloc(1<<n, sizeof(struct fib_node *));
	}

	free(terminals);
	free(terminal_tuple_ids);

	return G;
}
