#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <limits.h>
#include "hanan_grid.h"
#include "fib_heap.h"
#include "grid_prim.h"

// This is a "reserved" address that a (struct fib_node *) can point to if a (v,I) is in P
// instead of N. (I.e. no longer in the fibonacci heap)
uint8_t node_in_p_dummy;
#define NODE_IN_P ((struct fib_node *) &node_in_p_dummy)


struct triple {
	VertexId node;
	Set I;
	Coord lb;
};

Coord lb_1_tree(const struct grid *G, const VertexId v, const Set I)
{
	if (I == 0) return 0;

	struct tuple_id terminals_in_I[MAX_NUM_TERMINALS];
	uint_fast8_t num_terminals_in_I = 0;
	for (TerminalId i = 0; i < G->num_terminals; ++i)
		if (I & (1<<i))
			terminals_in_I[num_terminals_in_I++] =
				vertex_id_to_tuple_id(G, G->terminals[i]);

	const struct tuple_id v_tuple_id = vertex_id_to_tuple_id(G, v);

	if (num_terminals_in_I == 1) return dist(G, v_tuple_id, terminals_in_I[0]);

	Coord smallest_dist = COORD_MAX;
	Coord second_smallest_dist = COORD_MAX;
	for (uint_fast8_t i = 0; i < num_terminals_in_I; ++i) {
		const Coord i_v_dist =  dist(G, v_tuple_id, terminals_in_I[i]);
		if (i_v_dist <= smallest_dist) {
			second_smallest_dist = smallest_dist;
			smallest_dist = i_v_dist;
		} else if (i_v_dist < second_smallest_dist) {
			second_smallest_dist = i_v_dist;
		}
	}

	return (smallest_dist + second_smallest_dist + mst(G, I) + 1)/2; // ceil(.../2)
}

Coord lb_bb(const struct grid *G, const VertexId v, const Set I)
{
	if (I == 0) return 0;

	Coord min[DIM];
	Coord max[DIM];
	struct pos position = position_of_vertex_by_vertex_id(G, v);
	for (Dim d = 0; d < DIM; ++d) {
		min[d] = max[d] = position.coord[d];
	}
	for (TerminalId t = 0; t < G->num_terminals; ++t) {
		if (I & (1<<t)) {
			position = position_of_vertex_by_vertex_id(G, G->terminals[t]);
			for (Dim d = 0; d < DIM; ++d) {
				if (position.coord[d] < min[d]) min[d] = position.coord[d];
				if (position.coord[d] > max[d]) max[d] = position.coord[d];
			}
		}
	}
	Coord bb = 0;
	for (Dim d = 0; d < DIM; ++d) {
		bb += (max[d] - min[d]);
	}
	return bb;
}

Coord lb(const struct grid *G, const VertexId v, const Set I)
{
	const Coord onetree = lb_1_tree(G, v, I);
	const Coord bb = lb_bb(G, v, I);
	return (onetree > bb) ? onetree : bb;
}

//J \subset T\(I \cup t) iff
//J \subset T\t and J \cap I == \empty
bool check_cap(const Set J, const Set I, const Set Tt)
{
	return (Tt == (J|Tt)) && ((J&I) == 0) && (J != 0);
}

void Do6 (
	const struct grid *G,
	const struct fib_node *akt_fibnode,
	const struct triple *akt_trip,
	struct fib_heap *l
)
{
	const struct tuple_id id_v = vertex_id_to_tuple_id(G, akt_trip->node);
	for (Dim d = 0; d < DIM; ++d) {
		for (int i = -1; i <= 1; i += 2) {
			struct tuple_id id_w = id_v;
			if ((id_v.id[d]+i >= 0) && (id_v.id[d]+i < G->num_vertices[d])) {
				id_w.id[d] += i;
				const VertexId w = tuple_id_to_vertex_id(G, id_w);
				struct fib_node *fibnod = G->fib_sets[w][akt_trip->I];
				const Coord c = dist(G, id_v, id_w);
				if ((fibnod != NULL) && (fibnod != NODE_IN_P)) {
					const struct triple *fibnodval = fibnod->val;
					if (fibnod->key - fibnodval->lb > akt_fibnode->key - akt_trip->lb + c) {
						fib_heap_decrease_key(
							l,
							fibnod,
							akt_fibnode->key-akt_trip->lb + c + fibnodval->lb
						);
					}
				} else if (fibnod == NULL) {
					struct triple *p = malloc(sizeof(struct triple));
					p->node = w;
					p->I = akt_trip->I;
					p->lb = lb(G,p->node,p->I);
					G->fib_sets[p->node][p->I] = fib_heap_insert(
						l,
						p,
						akt_fibnode->key - akt_trip->lb + c + p->lb
					);
				}
			}
		}
	}
}

Set generate_sets(Set I, Set Tt, int num_term, size_t v)
{
	Tt = Tt^I;
	Set result = 0;
	int counter = 0;
	for (int i = 1; i < num_term; ++i) {
		if (Tt & (1<<i)) {
			if (v & (1<<counter))
				result += (1<<i);
			counter++;
		}
	}
	return result;
}

void Do7 (
	const struct grid *G,
	const struct fib_node *akt_fibnode,
	const struct triple *akt_trip,
	struct fib_heap *l,
	const Set T_without_t
)
{
	const uint_fast32_t num_subsets = (1 << __builtin_popcount(T_without_t - akt_trip->I));

	if (G->path_sets[akt_trip->node].num_pathsets < num_subsets) {
		const struct pathset *vector = G->path_sets[akt_trip->node].vector;
		for (uint_fast32_t j = 0; j< G->path_sets[akt_trip->node].num_pathsets; ++j) {	
			if (check_cap(vector[j].I, akt_trip->I, T_without_t)) { // J=vector[j].I subset of T\(I+t)
				struct fib_node *fibnod =
					G->fib_sets[akt_trip->node][akt_trip->I | vector[j].I];
				const struct triple *fibnodval = fibnod->val;
				if ((fibnod != NULL) && (fibnod != NODE_IN_P)) { // in heap
					if (fibnod->key - fibnodval->lb > akt_fibnode->key - akt_trip->lb + vector[j].key - vector[j].lb) {
						fib_heap_decrease_key(
							l,
							fibnod,
							akt_fibnode->key - akt_trip->lb + vector[j].key - vector[j].lb + fibnodval->lb
						);
					}
				} else if (fibnod == NULL) { // not in heap, will be added
					struct triple *p = malloc(sizeof(struct triple));
					p->node = akt_trip->node;
					p->I = (akt_trip->I | vector[j].I);
					p->lb = lb(G, p->node, p->I);
					G->fib_sets[p->node][p->I] = fib_heap_insert(
						l,
						p,
						akt_fibnode->key - akt_trip->lb + vector[j].key - vector[j].lb + p->lb
					);
				}
			}
		}
	} else {
		for (uint_fast32_t v = 1; v < num_subsets; ++v) {
			const Set J = generate_sets(akt_trip->I, T_without_t, G->num_terminals, v);
			if (G->index_of_pathsets[akt_trip->node][J] == 0) continue; // not in P

			const struct pathset pathJ =
				G->path_sets[akt_trip->node].vector[G->index_of_pathsets[akt_trip->node][J]-1];
			struct fib_node *fibnod = G->fib_sets[akt_trip->node][akt_trip->I | J];
			const struct triple *fibnodval = fibnod->val;
			if ((fibnod != NULL) && (fibnod != NODE_IN_P)) {
				if (fibnod->key - fibnodval->lb > akt_fibnode->key - akt_trip->lb + pathJ.key - pathJ.lb) {
					fib_heap_decrease_key(
						l,
						fibnod,
						akt_fibnode->key - akt_trip->lb + pathJ.key - pathJ.lb + fibnodval->lb
					);
				}
			} else if (fibnod == NULL) {
				struct triple *p = malloc(sizeof(struct triple));
				p->node = akt_trip->node;
				p->I = (akt_trip->I | J);
				p->lb = lb(G,p->node,p->I);
				G->fib_sets[p->node][p->I] = fib_heap_insert(
					l,
					p,
					akt_fibnode->key - akt_trip->lb + pathJ.key - pathJ.lb + p->lb
				);
			}
		}
	}
}

void dijkstra_steiner(const struct grid *G)
{
	// initialize P:
	for(VertexId v = 0; v < G->total_num_vertices; ++v) {
		add_to_P(G, v, 0, 0, 0);
	}

	const Set T_without_t = (1 << G->num_terminals) - 2;

	// Initialize l. Save l+lb as l.
	struct fib_heap l = {.n = 0, .b = NULL};
	for (TerminalId i = 1; i < G->num_terminals; ++i) {
		struct triple *p = malloc(sizeof(struct triple));
		p->node = G->terminals[i];
		p->I = 1<<i;
		p->lb = lb(G,p->node,p->I);
		G->fib_sets[G->terminals[i]][p->I] = fib_heap_insert(&l, p, p->lb);
	}

	while (true) {
		struct fib_node *akt_fibnode = fib_heap_extract_min(&l);
		struct triple *akt_trip = akt_fibnode->val;

		//Do 4, that is add our triple to the path
		add_to_P(G, akt_trip->node, akt_trip->I, akt_fibnode->key, akt_trip->lb);
		//Do 5
		if (akt_trip->node == G->terminals[0] && akt_trip->I == T_without_t) {
			printf("%d\n", akt_fibnode->key - akt_trip->lb);
			free(akt_trip);
			free(akt_fibnode);
			fib_heap_free_values(&l);
			fib_heap_free(&l);
			return;
		}
		//Do 6
		Do6(G, akt_fibnode, akt_trip, &l);
		//Do 7
		Do7(G, akt_fibnode, akt_trip, &l, T_without_t);
		//free fibnodes, set v as IN_P
		G->fib_sets[akt_trip->node][akt_trip->I] = NODE_IN_P;
		free(akt_trip);
		free(akt_fibnode);
	}
}

int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s grid.dimacs\n", argv[0]);
		return 1;
	}

	FILE *terminals_file = fopen(argv[1], "r");
	struct grid G = read_grid_from_file(terminals_file);
	fclose(terminals_file);
	if (G.num_terminals <= 1) {
		printf("%i\n",0);
		return 0;
	}

	dijkstra_steiner(&G);

	free_grid(&G);

	return 0;
}
