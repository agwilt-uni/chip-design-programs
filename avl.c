#include "avl.h"

int avl_max( int a , int b) {
	return (a>b)? a:b;
}

int height(struct avl* p) {
	if(p == NULL){
		return 0;
	}
	return p->height;
}

int avl_balance(struct avl* p){
	if(p==NULL) return 0;
	return height(p->left)-height(p->right);
}

struct avl* avl_find(struct avl* avl, uint32_t key){
	while(avl!= NULL && avl->key != key) {
		if(key<avl->key) avl = avl->left;
		else avl = avl->right;
	}
	return avl;
}

struct avl* rotateR(struct avl* p) {
	struct avl* v = p->left;
	struct avl* w = v->right;
	//rotate
	v->right= p;
	p->left = w;
	//heights:
	p->height = avl_max(height(p->left),height(p->right))+1;
	v->height = avl_max(height(v->left),height(v->right))+1;
	//Neue "Wurzel" zurück:
	return v;
}

struct avl* rotateL(struct avl* p) {
	struct avl* v = p->right;
	struct avl* w = v->left;
	//rotate
	v->left = p;
	p->right = w;
	//heights:
	p->height = avl_max(height(p->left),height(p->right))+1;
	v->height = avl_max(height(v->left),height(v->right))+1;
	//Neue "Wurzel" zurück:
	return v;
}

struct avl* insertrec(struct avl* avl, struct avl* node){
	if(avl==NULL) return node;
	if(node->key<avl->key){
		avl->left = insertrec(avl->left,node);
	}
	else if(node->key>avl->key){
		avl->right = insertrec(avl->right,node);
	}
	else {
		printf("Error: inserted node with key already in avl tree!\n");
		return NULL;
	}
	avl->height= avl_max(height(avl->left),height(avl->right))+1;
	int balance = avl_balance(avl);
	if(balance >1 && node->key < avl->left->key) return rotateR(avl);
	if(balance<-1 && node->key > avl->right->key) return rotateL(avl);
	if(balance>1 && node->key > avl->left->key) {
		avl->left = rotateL(avl->left);
		return rotateR(avl);
	}
	if(balance<-1 && node->key < avl->right->key) {
		avl->right = rotateR(avl->right);
		return rotateL(avl);
	}
	//else is balanced:
	return avl;
}
struct avl* avl_insert(struct avl* avl, uint32_t key, void* val){
	//allocate new node:
	struct avl* p = malloc(sizeof(struct avl));
	p->left = NULL;
	p->right = NULL;
	p->key = key;
	p->val = val;
	p->height = 1;
	return insertrec(avl,p);
}

struct avl* allleft(struct avl* avl){
	while(avl->left) {
		avl = avl->left;
	}
	return avl;
}

struct avl* avl_remove(struct avl* avl, int key) {
	if(avl==NULL){
		printf("Error: avl_remove of non existing key!\n");
		return avl;
	}
	if(key<avl->key) {
		avl->left = avl_remove(avl->left,key);
	}
	else if(key >avl->key){
		avl->right = avl_remove(avl->right,key);
	}
	else {//value found, go through cases
		if(avl->left != NULL && avl->right !=NULL){
			struct avl* n = allleft(avl->right);
			avl->key = n->key;
			avl->val = n->val;
			avl->right = avl_remove(avl->right,n->key);
		}
		else if(avl->left != NULL){
			struct avl* temp = avl->left;
			free(avl);
			avl = temp;
		}
		else if(avl->right !=NULL) {
			struct avl* temp = avl->right;
			free(avl);
			avl=temp;
		}
		else {
			free(avl);
			avl = NULL;
		}
	}
	
	if(avl==NULL) return avl;
	avl->height=avl_max(height(avl->left),height(avl->right))+1;
	int balance = avl_balance(avl);
	if(balance>1 && key>avl->left->key) return rotateR(avl);
	if(balance<-1 && key<avl->right->key) return rotateL(avl);
	if(balance >1 && key < avl->left->key){
		avl->left = rotateL(avl->left);
		return rotateR(avl);
	}
	if(balance<-1 && key > avl->right->key){
		avl->right = rotateR(avl->right);
		return rotateL(avl);
	}
	return avl;
}

