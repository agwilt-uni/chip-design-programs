#include "graph.h"
#include "util.h"

#include <stdlib.h>

/*
 * Vertex stuff
 */

void add_neighbour(struct vertex *v, const long w)
{
	if (! v->_max_num_neighbours) {
		v->neighbours = malloc(sizeof w);
		v->_max_num_neighbours = 1;
	} else while (v->_max_num_neighbours <= v->num_neighbours) {
		v->neighbours = reallocarray(
			v->neighbours,
			v->_max_num_neighbours <<= 1,
			sizeof w
		);
	}
	v->neighbours[v->num_neighbours++] = w;
}

bool vertices_adjacent(const struct vertex *x, long y)
{
	for (size_t i = 0; i < x->num_neighbours; ++i) {
		if (y == x->neighbours[i]) {
			return true;
		}
	}
	return false;
}

/*
 * Graph stuff
 */

void free_graph(struct graph *G)
{
	for (long v = 0; v < G->num_vertices; ++v)
		if (G->vertices[v]._max_num_neighbours)
			free(G->vertices[v].neighbours);
	free(G->vertices);
	G->num_vertices = 0;
	G->num_edges = 0;
}

void graph_add_edge(struct graph *G, const long x, const long y)
{
	if ((x==y) || (x<0) || (y<0) || (x >= G->num_vertices) || ( y >= G->num_vertices)) {
		fprintf(stderr, "Bad edge!\n");
		exit(1);
	}
	add_neighbour(G->vertices+x, y);
	add_neighbour(G->vertices+y, x);
	++G->num_edges;
}

struct graph read_graph_from_dimacs(FILE *stream)
{
	if (stream == NULL) {
		fprintf(stderr, "Invalid file stream.\n");
		exit(1);
	}

	// Navigate to first "p edge ....." line
	while (getc(stream) != 'p') {
		while (getc(stream) != '\n');
	}

	long n, m;
	if (fscanf(stream, " edge %ld %ld\n", &n, &m) != 2) {
		fprintf(stderr, "Bad first line!\n");
		exit(1);
	}
	struct graph G = {calloc(n, sizeof(struct vertex)), n, 0};
	bool *has_set_vertex_position = calloc(n, sizeof(bool));

	char *line = NULL;
	size_t len = 0;
	while (getline(&line, &len, stream) != -1) {
		if (line[0] == 'c') {
			continue;
		} else if (line[0] == 'e') {
			// convert edge from DIMACS
			const char *p = line+2;
			const long x = fast_atol(&p) - 1;
			++p;
			const long y = fast_atol(&p) - 1;
			if ((x == y) || (x < 0) || (y < 0) || (x >= n) || (y >= n)) {
				fprintf(stderr, "Bad edge: %ld %ld\n", x+1, y+1);
				exit(1);
			}

			graph_add_edge(&G, x, y);
		} else if (line[0] == 'n') {
			// read vertex position
			const char *p = line+2;
			const long v = fast_atol(&p) - 1;
			++p;
			G.vertices[v].pos = atol(p);
			if (has_set_vertex_position[v]) {
				fprintf(stderr, "Position of vertex %ld defined twice\n", v+1);
				exit(1);
			}
			has_set_vertex_position[v] = true;
		} else {
			fprintf(stderr, "Bad DIMACS line!\n");
			exit(1);
		}
	}
	free(line);

	if (G.num_edges != m) {
		fprintf(stderr, "Wrong number of edges!\n");
		exit(1);
	}
	for (long v = 0; v < n; ++v) {
		if (! has_set_vertex_position[v]) {
			fprintf(stderr, "Position of vertex %ld not defined\n", v+1);
			exit(1);
		}
	}
	free(has_set_vertex_position);

	return G;
}
