#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>
#include "graph.h"
#include "gurobi_c.h"

static GRBenv *env = NULL;
static GRBmodel *model = NULL;

double compute_netlength(const struct graph *G, const double *placement, const int p)
{
	double netlength = 0.0;
	for (long v = 0; v < G->num_vertices; ++v) {
		for (size_t i = 0; i < G->vertices[v].num_neighbours; ++i) {
			const double neighbour_pos = placement[G->vertices[v].neighbours[i]];
			if (placement[v] >= neighbour_pos) {
				netlength += pow(placement[v] - neighbour_pos, p);
			}
		}
	}
	return netlength;
}

long compute_integral_netlength(const struct graph *G, const long *placement, const int p)
{
	long netlength = 0.0;
	for (long v = 0; v < G->num_vertices; ++v) {
		for (size_t i = 0; i < G->vertices[v].num_neighbours; ++i) {
			const long neighbour_pos = placement[G->vertices[v].neighbours[i]];
			if (placement[v] >= neighbour_pos) {
				netlength += pow(placement[v] - neighbour_pos, p);
			}
		}
	}
	return netlength;
}

void print_placement_info(const struct graph *G, const double *placement, const bool truncate)
{
	for (long v = 0; v < G->num_vertices; ++v) {
		if (truncate)
			printf("%ld %.0lf\n", v+1, placement[v]);
		else
			printf("%ld %lf\n", v+1, placement[v]);
	}
	if (truncate) {
		printf("Linear netlength: %.0lf\n", compute_netlength(G, placement, 1));
		printf("Quadratic netlength: %.0lf\n", compute_netlength(G, placement, 2));
	} else {
		printf("Linear netlength: %lf\n", compute_netlength(G, placement, 1));
		printf("Quadratic netlength: %lf\n", compute_netlength(G, placement, 2));
	}
}

void print_integral_placement_info(const struct graph *G, const long *placement)
{
	for (long v = 0; v < G->num_vertices; ++v) {
		printf("%ld %ld\n", v+1, placement[v]);
	}
	printf("Linear netlength: %ld\n", compute_integral_netlength(G, placement, 1));
	printf("Quadratic netlength: %ld\n", compute_integral_netlength(G, placement, 2));
}

double *compute_min_dist_squared_positions(struct graph *G)
{
	double *placement = malloc(G->num_vertices * sizeof(double));

	long *vertices_to_place = malloc(G->num_vertices * sizeof(long));
	size_t num_vertices_to_place = 0;

	// Initial placement
	for (long v = 0; v < G->num_vertices; ++v) {
		const long pos = G->vertices[v].pos;
		if (pos == -1) {
			vertices_to_place[num_vertices_to_place++] = v;
			// TODO: Think up some really clever heuristic for start positions
			placement[v] = G->num_vertices/2.0;
		} else {
			placement[v] = pos;
		}
	}

	double *movement = malloc(num_vertices_to_place * sizeof(double));

	double max_circuit_movement = 1.0;
	while (max_circuit_movement >= 0.1) {
		// Compute movements
		for (size_t i = 0; i < num_vertices_to_place; ++i) {
			const long v = vertices_to_place[i];
			double x = 0.0;
			for (size_t j = 0; j < G->vertices[v].num_neighbours; ++j) {
				x += placement[G->vertices[v].neighbours[j]];
			}
			movement[i] = x/G->vertices[v].num_neighbours - placement[v];
		}
		// Apply movements
		max_circuit_movement = 0.0;
		for (size_t i = 0; i < num_vertices_to_place; ++i) {
			const long v = vertices_to_place[i];
			if (fabs(movement[i]) > max_circuit_movement)
				max_circuit_movement = fabs(movement[i]);
			placement[v] += movement[i];
		}
		//printf("Max Circuit Movement: %lf\n", max_circuit_movement);
	}

	free(vertices_to_place);
	free(movement);
	return placement;
}


double* compute_min_linear_dist_positions(struct graph* G){
	/* We solve the following LP (note that it is TDI):
	 * min \sum_{e \in E} d_e constrained by:
	 *	g(v)-g(w) \le d_e		for all v,w \in V and e \in E
	 *	g(w)-g(v) \le d_e		for all v,w \in V and e \in E
	 *	g(v) = f(v)				for all v \in V\C
	 *
	 * This yields the desired minimizing of the L_1 distance, because for any minimum we have d_e = |g(v)-g(w)|.
	 * Also we see, that for any optimum of the above LP we can round down the position g(v) without changing the objective function. 
	 * (if moving the position g(v) to the left would make it worse, moving it right would make it better contradicting the optimality.)
	 * 
	 * */
	int k = G->num_vertices;
	int m = G->num_edges;
	int error = 0;
	double *minPositions = malloc(k*sizeof(double));

	/* Create initial model */
	error = GRBnewmodel(env, &model, "positions", k+m,
                      NULL, NULL, NULL, NULL, NULL);
	if (error) goto QUIT;
	/* Set variables*/
	//nodes
	for(int j = 0; j<k;++j){
		error = GRBsetdblattrelement(model, "Obj", j, 0);
		if (error) goto QUIT;
	}
	//edges
	for(int j = k; j<m+k;++j){
		error = GRBsetdblattrelement(model, "Obj", j, 1);
		if (error) goto QUIT;
	}
	error = GRBsetintattr(model, "ModelSense", GRB_MINIMIZE);
	if (error) goto QUIT;
	/*add constraints that fix f(v)*/
	int indi;
	double vali;
	for(int i = 0;i<k;++i){
		if(G->vertices[i].pos != -1){
			indi=i;
			vali=1;
			error =	GRBaddconstr(model,1,&indi,&vali,GRB_EQUAL,G->vertices[i].pos,NULL);
			if(error) goto QUIT;
		}
	}
	/*add constraints for edges*/
	//edges are orderd, by going through vertices only to vertices with higher numbers
	int ind[3];
	double val[3];
	int counter = k;
	for(int i=0;i<k-1;++i){
		for(size_t j = 0; j<G->vertices[i].num_neighbours;j++){
			if(i<G->vertices[i].neighbours[j]){
				ind[0]=i;
				ind[1]=G->vertices[i].neighbours[j];
				ind[2]=counter;
				val[0]=1;
				val[1]=-1;
				val[2]=-1;
				error = GRBaddconstr(model,3,ind,val,GRB_LESS_EQUAL,0,NULL);
				if(error) goto QUIT;
				val[1]=1;
				val[0]=-1;
				error = GRBaddconstr(model,3,ind,val,GRB_LESS_EQUAL,0,NULL);
				if(error) goto QUIT;
				counter++;
			}
		}
	}
	/* Solve */
	error = GRBoptimize(model);
	if(error) goto QUIT;
	double obj;
	error = GRBgetdblattr(model,"ObjVal",&obj);
	//printf("best linear: %lf \n",obj);
	if(error) goto QUIT;
	double x;
	//grab the solution from gurobi and put it in minPositions
	for(int i = 0; i<k;++i){
		error = GRBgetdblattrelement(model,"X",i,&x);
		if(error) goto QUIT;
		//printf("%lf \n",x);
		minPositions[i]=floor(x);
	}
	QUIT:

		/* Error reporting */

	if (error)
	{
		printf("ERROR: %s\n", GRBgeterrormsg(env));
		exit(1);
	}

	/* Free model */

	GRBfreemodel(model);

	return minPositions;
}

long* place(struct graph* G, const double* preposition){
	/* Becasue it doesnt make any difference if we call "place" recursive on itself or just order the vertices to be placed and the free positions
	 * and then assign the the values to the corresponding ones, we will do the later for performance reasons.
	 * This effectifly only changes the order in which we assign the vertices, so the output stays the same.
	 * (I really dont know why one would do this recursivley)
	 * */
	 

	int k = G->num_vertices;
	int placed = 0;
	long* placement = calloc(k,sizeof(long)); // the physical layout is saved here
	//initialize placed and placement through f
	for(int i = 0; i<k;++i){
		if(G->vertices[i].pos!=-1) {
			placed++;
			placement[G->vertices[i].pos-1]=i+1; //in placement id+1 is saved
		}
	}
	//printf("%ld \n",placement[0]);
	long* to_place = malloc((k-placed)*sizeof(long)); //stores the IDs of the nodes in C
	int* free_positions = malloc((k-placed)*sizeof(int)); //stores the available positions
	//fill to_place 
	int counter = 0;
	for(int i = 0;i<k;i++){
		if(G->vertices[i].pos==-1){
			to_place[counter]=i;
			counter++;
		}
	}
	//sort to_place to get the median in const time [I know inefficient but i dont know any c libraries]
	for(int i = 1; i<k-placed;++i){
		for(int j = 0; j<k-placed-i;++j){
			if(preposition[to_place[j]]>preposition[to_place[j+1]]){
				to_place[j]+=to_place[j+1];
				to_place[j+1] = to_place[j]-to_place[j+1];
				to_place[j]-=to_place[j+1];
			}
		}
	}
	//initialise free positions to get their median
	counter = 0;
	for(int i = 0; i<k;++i){
		if(placement[i]==0) free_positions[counter++]=i;
	}
	//place the shit
	counter=(k-placed)/2;
	for(int i = 1;i<=k-placed;++i){
		//printf("placing, counter= %i \n", counter);
		placement[free_positions[counter]]=to_place[counter]+1;
		counter+= (1-2*(i%2))*i;
	}
	free(free_positions);
	free(to_place);
	long* nodes = malloc(k*sizeof(long)); //changes placement from physical layout(i.e. position->ID) to ID->position
	for(int i=0;i<k;++i){
		nodes[placement[i]-1]=i+1;
	}
	free(placement);
	return nodes;
}


int main(int argc, char *argv[])
{
	if (argc < 2) {
		fprintf(stderr, "Usage: %s graph.dimacs\n", argv[0]);
		return 1;
	}
	FILE *graph_file = fopen(argv[1], "r");
	struct graph G = read_graph_from_dimacs(graph_file);
	fclose(graph_file);

	/* Create environment */
	int error = GRBloadenv(&env, NULL);
	if (error) {
		printf("ERROR: %s\n", GRBgeterrormsg(env));
		return 1;
	}
	GRBsetintparam(env,"OutputFlag",0);

	printf("Initial Positions:\n");
	for (long v = 0; v < G.num_vertices; ++v) {
		if (G.vertices[v].pos != -1) {
			printf("%ld %ld\n", v+1, G.vertices[v].pos);
		}
	}

	double *placement_a = compute_min_dist_squared_positions(&G);
	printf("\n(a)\n");
	print_placement_info(&G, placement_a, false);

	double *placement_b = compute_min_linear_dist_positions(&G);
	printf("\n(b)\n");
	print_placement_info(&G, placement_b, true);

	long *placement_c_a = place(&G,placement_a);
	printf("\n(c) using (a)\n");
	print_integral_placement_info(&G, placement_c_a);

	long *placement_c_b = place(&G,placement_b);
	printf("\n(c) using (b)\n");
	print_integral_placement_info(&G, placement_c_b);

	free(placement_a);
	free(placement_b);
	free(placement_c_a);
	free(placement_c_b);

	free_graph(&G);

	//free GRBenv
	GRBfreeenv(env);

	return 0;
}
