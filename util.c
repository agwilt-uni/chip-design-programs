#include "util.h"

long fast_atol(const char **str)
{
	long val = 0;
	while ( !(**str == ' ' || **str == '\n' || **str == '\0')) {
		val = val*10 + (*(*str)++ - '0');
	}
	return val;
}
