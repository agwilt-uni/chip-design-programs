CC = clang
CXX = clang++
#CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native -g -pg
CCFLAGS = -Wall -Wextra -O3 -pedantic -march=native -mtune=native
CXXFLAGS = $(CCFLAGS)

DEPS = Makefile
OBJ = obj

$(OBJ)/%.o: %.c %.h $(DEPS)
	mkdir -p $(OBJ)
	$(CC) -c -o $@ $< $(CCFLAGS)

$(OBJ)/%.o: %.cpp %.hpp $(DEPS)
	mkdir -p $(OBJ)
	$(CXX) -c -o $@ $< $(CXXFLAGS)

all: p01 p02

p02: p02.c $(OBJ)/util.o $(OBJ)/hanan_grid.o $(OBJ)/fib_heap.o $(OBJ)/grid_prim.o
	$(CC) $(CCFLAGS) p02.c $(OBJ)/hanan_grid.o $(OBJ)/util.o $(OBJ)/fib_heap.o $(OBJ)/grid_prim.o -o p02

test: test.c $(OBJ)/util.o $(OBJ)/hanan_grid.o $(OBJ)/fib_heap.o
	$(CC) $(CCFLAGS) test.c $(OBJ)/hanan_grid.o $(OBJ)/util.o $(OBJ)/fib_heap.o -o test

p01: p01.c $(OBJ)/util.o $(OBJ)/graph.o
	$(CC) $(CCFLAGS) -lm -lgurobi90 p01.c $(OBJ)/graph.o $(OBJ)/util.o -o p01

.PHONY: clean bin_clean obj_clean
clean: bin_clean obj_clean
bin_clean:
	rm -fv p01 p02 test
obj_clean:
	rm -rfv $(OBJ)/*
