#ifndef GRID_H
#define GRID_H

#include <limits.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include "fib_heap.h"

#define MAX_NUM_TERMINALS 32
#define DIM 3 // WARNING: SequentialId must have at least log2(MAX_NUM_TERMINAL)*DIM bits
typedef uint32_t Set;
typedef int Coord;
#define COORD_MAX INT_MAX
typedef uint16_t VertexId;
typedef uint_fast8_t TerminalId;
typedef int_fast8_t Dim;

/*
 * Grid structure
 */

struct grid {
	VertexId num_vertices[DIM];
	Coord *positions[DIM]; // sorted
	TerminalId num_terminals;
	VertexId terminals[MAX_NUM_TERMINALS];
	// indexed by vertex id:
	struct pathsetvector* path_sets;
	size_t** index_of_pathsets; //denotes index of pathset +1 !!!!
	struct fib_node ***fib_sets;
	// make sure these are correct:
	VertexId total_num_vertices; // = num_vertices[0] * num_vertices[1] * ...
	// for more speed
	Coord terminal_dist[MAX_NUM_TERMINALS][MAX_NUM_TERMINALS];
};

void print_grid(const struct grid *G);
void free_grid(struct grid *G);
struct grid read_grid_from_file(FILE *stream);

/*
 * Vertex Stuff
 */

struct pos {
	Coord coord[DIM];
};

struct tuple_id {
	uint8_t id[DIM];
};

VertexId tuple_id_to_vertex_id(const struct grid *G, struct tuple_id v);
struct tuple_id vertex_id_to_tuple_id(const struct grid *G, VertexId vertex_id);

struct pos position_of_vertex_by_tuple_id(const struct grid *G, struct tuple_id v);
struct pos position_of_vertex_by_vertex_id(const struct grid *G, VertexId v);

/*
 * Mixed Stuff
 */

Coord dist(const struct grid *G, struct tuple_id v, struct tuple_id w);

/*
 * Pathsets
 */

struct pathset {
	Set I;
	Coord key;
	Coord lb;
};

struct pathsetvector {
	struct pathset* vector;
	size_t num_pathsets;
	size_t max_num_pathsets;
};

void add_pathsetvector(struct pathsetvector* P, struct pathset w);
void add_to_P(const struct grid* G, VertexId v, Set I, Coord key, Coord lb);

#endif // GRID_H
