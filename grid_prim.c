#include "grid_prim.h"

#include <math.h>

Coord mst(const struct grid *G, Set S)
{
	if (S == 0)
		return 0;

	TerminalId terminals_in_S[MAX_NUM_TERMINALS];
	uint_fast8_t card_S = 0;
	for (TerminalId v = 0; v < G->num_terminals; ++v)
		if (S & (1<<v)) terminals_in_S[card_S++] = v;
	TerminalId v = terminals_in_S[0];

	Set Y = (1 << v); // Initial tree contains only v

	Coord cost[MAX_NUM_TERMINALS];
	for (TerminalId v = 0; v < G->num_terminals; ++v) cost[v] = COORD_MAX;
	cost[v] = 0;

	while (Y != S) {
		// go through all edges from v to S/Y
		for (uint_fast8_t i = 0; i < card_S; ++i) {
			const TerminalId w = terminals_in_S[i];
			if (Y & (1<<w)) continue;

			if (G->terminal_dist[v][w] < cost[w]) cost[w] = G->terminal_dist[v][w];
		}
		// get new v
		Coord min_cost = COORD_MAX;
		for (uint_fast8_t i = 0; i < card_S; ++i) {
			const TerminalId w = terminals_in_S[i];
			if (Y & (1<<w)) continue;
			if (cost[w] < min_cost) {
				min_cost = cost[w];
				v = w;
			}
		}
		// add new v to tree
		Y |= (1 << v);
	}

	Coord total_cost = 0;
	for (uint_fast8_t i = 0; i < card_S; ++i) total_cost += cost[terminals_in_S[i]];
	return total_cost;
}
