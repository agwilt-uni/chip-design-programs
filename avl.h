#ifndef AVL_H
#define AVL_H
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>


struct avl {
	struct avl* left;
	struct avl* right;
	uint32_t key;
	void* val;
	int height;
};


//return the node where the key is equal, or NULL
struct avl* avl_find(struct avl* avl, uint32_t key); 

struct avl* avl_insert(struct avl* avl, uint32_t key, void* val);

//frees the node, but not the pointer to val!! (good in our case, as we only want to refference into the fib_heap)
struct avl* avl_remove(struct avl* avl, int key); 

#endif
